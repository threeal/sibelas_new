import 'package:flutter/material.dart';
import 'package:sibelas/pembeli_home.dart';
import 'package:sibelas/pembeli_search.dart';

class HomePagePembeli extends StatefulWidget {
  String jKegunaan = "Semua Kegunaan";
  String jHarga = "Semua Harga";

  HomePagePembeli(String kegunaan, String harga)
  {
    jKegunaan = kegunaan;
    jHarga = harga;
  }

  @override
  _HomePagePembeliState createState() => _HomePagePembeliState(jKegunaan, jHarga);
}

class _HomePagePembeliState extends State<HomePagePembeli>
  with SingleTickerProviderStateMixin {

    String jKegunaan = "Semua Kegunaan";
    String jHarga = "Semua Harga";

    _HomePagePembeliState(String kegunaan, String harga)
    {
      jKegunaan = kegunaan;
      jHarga = harga;
    }

    TabController controller;

    @override
    void initState() {
      controller = new TabController(vsync: this, length: 2);
      super.initState();
    }

    @override
    void dispose() {
      controller.dispose();
      super.dispose();
    }

    @override
    Widget build(BuildContext context) {
      return Scaffold(
        appBar: AppBar(
          title: Text('SiBELAS'),
          backgroundColor: Colors.blueAccent[700],
          automaticallyImplyLeading: false,
        ),
        body: TabBarView(
          controller: controller,
          children: <Widget>[
            Home(jKegunaan, jHarga),
            Search(jKegunaan, jHarga),
          ],
        ),
        bottomNavigationBar: Material(
          color: Colors.lightBlueAccent[700],
          child: TabBar(
          controller: controller,
            tabs: <Widget>[
              Tab(
                icon: Icon(Icons.home),
              ),
              Tab(
                icon: Icon(Icons.search),
              ),
            ],
        ),
      ),
    );
  }
}
