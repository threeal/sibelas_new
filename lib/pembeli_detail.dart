import 'package:flutter/material.dart';
import 'package:firebase_storage/firebase_storage.dart';

class Detail extends StatefulWidget {
  final List list;
  final int index;
  Detail({this.list, this.index});
  @override
  _DetailState createState() => _DetailState();
}

class _DetailState extends State<Detail> {
  Future<Widget> _getImage(BuildContext context, String image) async {
    Image img;

    var ref = FirebaseStorage.instance.ref().child(image);
    await ref.getDownloadURL().then((url) {
      if (url != null) {
        img = Image.network(
          url.toString(),
          fit: BoxFit.scaleDown,
        );
      } else {
        img = Image.asset(
          "images/logo.png",
          width: 100.0,
        );
      }
    });

    return img;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("${widget.list[widget.index]['nama']}"),
      ),
      body: Container(
        padding: const EdgeInsets.all(20.0),
        child: Center(
          child: ListView(
            children: <Widget>[
              FutureBuilder(
                future: _getImage(context, widget.list[widget.index]['foto']),
                builder: (context, snapshot) {
                  if (snapshot.connectionState == ConnectionState.done) {
                    return Container(
                      height: MediaQuery.of(context).size.height / 1.25,
                      width: MediaQuery.of(context).size.width / 1.25,
                      child: snapshot.data,
                    );
                  }

                  if (snapshot.connectionState == ConnectionState.waiting) {
                    return Container(
                      height: MediaQuery.of(context).size.height / 1.25,
                      width: MediaQuery.of(context).size.width / 1.25,
                      child: CircularProgressIndicator()
                    );
                  }

                  return Container();
                }
              ),
              Text(
                widget.list[widget.index]['nama'],
                style: TextStyle(fontSize: 20.0),
                textAlign: TextAlign.center,
              ),
              Text(
                "Rp ${widget.list[widget.index]['harga']}",
                style: TextStyle(fontSize: 20.0),
                textAlign: TextAlign.center,
              ),
              SizedBox(height: 20.0,),
              Text(
                widget.list[widget.index]['spesifikasi'],
                style: TextStyle(fontSize: 20.0),
                textAlign: TextAlign.center,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
