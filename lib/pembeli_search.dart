import 'package:flutter/material.dart';
import 'package:sibelas/home_page_pembeli.dart';

class Search extends StatefulWidget {
  String jKegunaan = "Semua Kegunaan";
  String jHarga = "Semua Harga";

  Search(String kegunaan, String harga)
  {
    jKegunaan = kegunaan;
    jHarga = harga;
  }

  @override
  _SearchState createState() => _SearchState(jKegunaan, jHarga);
}

class _SearchState extends State<Search> {
  List<String> jeniskegunaan = ["Semua Kegunaan", "Bisnis", "Desain", "Gaming"];
  String jkegunaan = "Semua Kegunaan";

  List<String> jenisharga = ["Semua Harga", "Dibawah 4 juta", "4 - 6 juta", "Diatas 6 juta"];
  String jharga = "Semua Harga";

  _SearchState(String kegunaan, String harga)
  {
    jkegunaan = kegunaan;
    jharga = harga;
  }

  void pilihkegunaan(String value) {
    setState(() {
      jkegunaan = value;
    });
  }

  void pilihharga(String value) {
    setState(() {
      jharga = value;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
      child: Center(
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 30.0, horizontal: 20.0),
          child: ListView(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Text(
                    'Kegunaan : ',
                    style: TextStyle(fontSize: 20.0, color: Colors.blueAccent)
                  ),
                  DropdownButton(
                    onChanged: (String value) {
                      pilihkegunaan(value);
                    },
                    value: jkegunaan,
                    items: jeniskegunaan.map((String value) {
                      return DropdownMenuItem(
                        value: value,
                        child: Text(value),
                      );
                    }).toList(),
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Text(
                    'Harga : ',
                    style: TextStyle(fontSize: 20.0, color: Colors.blueAccent)
                  ),
                  DropdownButton(
                    onChanged: (String value) {
                      pilihharga(value);
                    },
                    value: jharga,
                    items: jenisharga.map((String value) {
                      return DropdownMenuItem(
                        value: value,
                        child: Text(value),
                      );
                    }).toList(),
                  ),
                ],
              ),
              Padding(padding: EdgeInsets.symmetric(vertical: 20.0),),
              MaterialButton(
                color: Colors.blueAccent,
                onPressed: (){
                  Navigator.pushReplacement(
                    context,
                    MaterialPageRoute(builder: (BuildContext context) => HomePagePembeli(jkegunaan, jharga))
                  );
                },
                child: Text("Cari", style: TextStyle(color: Colors.white, fontSize: 15.0),),
              )
            ],
          ),
        ),
      ),
    ));
  }
}
