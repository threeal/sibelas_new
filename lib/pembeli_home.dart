import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:sibelas/pembeli_detail.dart';

class Home extends StatefulWidget {
  String jKegunaan = "Semua Kegunaan";
  String jHarga = "Semua Harga";

  Home(String kegunaan, String harga)
  {
    jKegunaan = kegunaan;
    jHarga = harga;
  }

  @override
  _HomeState createState() => _HomeState(jKegunaan, jHarga);
}

class _HomeState extends State<Home> {
  String jKegunaan = "Semua Kegunaan";
  String jHarga = "Semua Harga";

  _HomeState(String kegunaan, String harga)
  {
    jKegunaan = kegunaan;
    jHarga = harga;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: StreamBuilder<QuerySnapshot>(
        stream: Firestore.instance.collection("laptop").snapshots(),
        builder: (context, snapshot) {
          if (snapshot.hasError)
            print(snapshot.error);

          if (snapshot.hasData) {
            return ItemList(
              list: snapshot.data.documents,
              jKegunaan: jKegunaan,
              jHarga: jHarga,
            );
          } else {
            return Center(child: CircularProgressIndicator());
          }
        },
      )
    );
  }
}

class ItemList extends StatelessWidget {
  List list;
  String jKegunaan = "Semua Kegunaan";
  String jHarga = "Semua Harga";

  ItemList({
    this.list,
    this.jKegunaan,
    this.jHarga
  }) {
    List newlist = List<DocumentSnapshot>();
    list.forEach((l) {
      if (jHarga != "Semua Harga") {
        if (jHarga == "Dibawah 4 juta" && l['harga'] <= 4000000) {
          newlist.add(l);
        } else if (jHarga == "4 - 6 juta" && l['harga'] >= 4000000 && l['harga'] <= 6000000) {
          newlist.add(l);
        } else if (jHarga == "Diatas 6 juta" && l['harga'] >= 6000000) {
          newlist.add(l);
        }
      } else {
        newlist.add(l);
      }
    });
    list = newlist;
  }

  Future<Widget> _getImage(BuildContext context, String image) async {
    Image img;

    var ref = FirebaseStorage.instance.ref().child(image);
    await ref.getDownloadURL().then((url) {
      if (url != null) {
        img = Image.network(
          url.toString(),
          fit: BoxFit.scaleDown,
        );
      } else {
        img = Image.asset(
          "images/logo.png",
          width: 100.0,
        );
      }
    });

    return img;
  }

  @override
  Widget build(BuildContext context) {
    return GridView.builder(
      itemCount: list == null ? 0 : list.length,
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 2,
      ),
      itemBuilder: (context, i) {
        return Container(
          padding: const EdgeInsets.all(5.0),
          child: GestureDetector(
            onTap: () => Navigator.of(context).push(MaterialPageRoute(
              builder: (BuildContext context) => Detail(
                  list: list,
                  index: i,
                )
              )
            ),
            child: Card(
              child: Center(
                child: GridTile(
                  child: Center(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        LimitedBox(
                          maxHeight: 100.0,
                          maxWidth: 100.0,
                          child: FutureBuilder(
                            future: _getImage(context, list[i]['foto']),
                            builder: (context, snapshot) {
                              if (snapshot.connectionState == ConnectionState.done) {
                                return Container(
                                  height: MediaQuery.of(context).size.height / 1.25,
                                  width: MediaQuery.of(context).size.width / 1.25,
                                  child: snapshot.data,
                                );
                              }

                              if (snapshot.connectionState == ConnectionState.waiting) {
                                return Container(
                                  height: MediaQuery.of(context).size.height / 1.25,
                                  width: MediaQuery.of(context).size.width / 1.25,
                                  child: CircularProgressIndicator()
                                );
                              }

                              return Container();
                            },
                          ),
                        ),
                        Text(
                          list[i]['nama'],
                          textAlign: TextAlign.center,
                        ),
                        Text(
                          "Rp ${list[i]['harga']}",
                          textAlign: TextAlign.center,
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}
